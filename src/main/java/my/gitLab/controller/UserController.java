package my.gitLab.controller;

import my.gitLab.entity.User;
import my.gitLab.service.UserService;
import my.gitLab.service.impl.UserServiceImpl;
import my.gitLab.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UTFDataFormatException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    @PostMapping("/my/list")
    @ResponseBody
    public UserVo selectList(UserVo user){
        System.out.println("进入方法");
        return userService.userList(user);
    }
}

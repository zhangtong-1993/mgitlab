package my.gitLab.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 应用ID
     **/
    private String clientId;
    /**
     * 统计时间
     * */
    private Date countTime;
    /**
     * 次数
     * */
    private Integer count;

    /**
     *服务
     * */
    private String sysServiceId;

    /**
     * 用户名
     * */
    private String username;

    /**
     * 数据生成时间
     */
    private Date insertTime;

    private String serviceName;

    private String clientName;

    private String companyName;

    private String nickName;
}

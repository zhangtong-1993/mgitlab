package my.gitLab.service.impl;

import my.gitLab.dao.UserDao;
import my.gitLab.entity.User;
import my.gitLab.service.UserService;
import my.gitLab.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    public UserVo userList(UserVo user) {
        /*user.setName("zhangtong");
        user.setPassWord("zhangtong");*/
        UserVo userVo1 = userDao.UserList(user);
        return userVo1;
    }
}

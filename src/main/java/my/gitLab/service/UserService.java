package my.gitLab.service;

import my.gitLab.entity.User;
import my.gitLab.vo.UserVo;

public interface UserService {

    UserVo userList (UserVo user);
}

package my.gitLab.dao;

import my.gitLab.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {
    UserVo UserList (UserVo user);
}

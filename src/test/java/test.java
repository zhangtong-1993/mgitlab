import com.esgcc.AppConfig;
import com.esgcc.Boy;
import com.esgcc.Mei;
import com.esgcc.application;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.aspectj.util.TypeSafeEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = application.class)
public class test {
    public void Test() {

    }

    @Test
    public void ess() {
        //AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        Boy boy = context.getBean("boy", Boy.class);
        Mei girl = (Mei) context.getBean("mei");
        String s = boy.s(20.3);
        System.out.println(s);
        String s1 = girl.s(50.0);
        System.out.println(s1);

        /*List<Object> list = new ArrayList<>();
        List<Object> list1 = new ArrayList<>();
        list.add("j");
        list.add("x");
        list.add("f");
        list.add("b");
        List<Object> collect = list.stream().map(String::valueOf).collect(Collectors.toList());
        System.out.println(collect);*/
    }
/*    // 相当于AspectJ一共提供了24中之多（当然不包含Spring自己的bean的模式）
    public static final class PointcutPrimitive extends TypeSafeEnum {

        public final PointcutPrimitive CALL = new PointcutPrimitive("call",1);
        public final PointcutPrimitive EXECUTION = new PointcutPrimitive("execution",2);
        public final PointcutPrimitive GET = new PointcutPrimitive("get",3);
        public static final PointcutPrimitive SET = new PointcutPrimitive("set",4);
        public static final PointcutPrimitive INITIALIZATION = new PointcutPrimitive("initialization",5);
        public static final PointcutPrimitive PRE_INITIALIZATION = new PointcutPrimitive("preinitialization",6);
        public static final PointcutPrimitive STATIC_INITIALIZATION = new PointcutPrimitive("staticinitialization",7);
        public static final PointcutPrimitive HANDLER = new PointcutPrimitive("handler",8);
        public static final PointcutPrimitive ADVICE_EXECUTION = new PointcutPrimitive("adviceexecution",9);
        public static final PointcutPrimitive WITHIN = new PointcutPrimitive("within",10);
        public static final PointcutPrimitive WITHIN_CODE = new PointcutPrimitive("withincode",11);
        public static final PointcutPrimitive CFLOW = new PointcutPrimitive("cflow",12);
        public static final PointcutPrimitive CFLOW_BELOW = new PointcutPrimitive("cflowbelow",13);
        public static final PointcutPrimitive IF = new PointcutPrimitive("if",14);
        public static final PointcutPrimitive THIS = new PointcutPrimitive("this",15);
        public static final PointcutPrimitive TARGET = new PointcutPrimitive("target",16);
        public static final PointcutPrimitive ARGS = new PointcutPrimitive("args",17);
        public static final PointcutPrimitive REFERENCE = new PointcutPrimitive("reference pointcut",18);
        public static final PointcutPrimitive AT_ANNOTATION = new PointcutPrimitive("@annotation",19);
        public static final PointcutPrimitive AT_THIS = new PointcutPrimitive("@this",20);
        public static final PointcutPrimitive AT_TARGET = new PointcutPrimitive("@target",21);
        public static final PointcutPrimitive AT_ARGS = new PointcutPrimitive("@args",22);
        public static final PointcutPrimitive AT_WITHIN = new PointcutPrimitive("@within",23);
        public static final PointcutPrimitive AT_WITHINCODE = new PointcutPrimitive("@withincode",24);

        private PointcutPrimitive(String name, int key) {
            super(name, key);
        }*/

    @Test
    public void exportExcel() {
        String name = "nihao";
        List<List<String>> paramList = new LinkedList<>();
        HttpServletResponse response = new HttpServletResponse() {
            @Override
            public void addCookie(Cookie cookie) {

            }

            @Override
            public boolean containsHeader(String s) {
                return false;
            }

            @Override
            public String encodeURL(String s) {
                return null;
            }

            @Override
            public String encodeRedirectURL(String s) {
                return null;
            }

            @Override
            public String encodeUrl(String s) {
                return null;
            }

            @Override
            public String encodeRedirectUrl(String s) {
                return null;
            }

            @Override
            public void sendError(int i, String s) throws IOException {

            }

            @Override
            public void sendError(int i) throws IOException {

            }

            @Override
            public void sendRedirect(String s) throws IOException {

            }

            @Override
            public void setDateHeader(String s, long l) {

            }

            @Override
            public void addDateHeader(String s, long l) {

            }

            @Override
            public void setHeader(String s, String s1) {

            }

            @Override
            public void addHeader(String s, String s1) {

            }

            @Override
            public void setIntHeader(String s, int i) {

            }

            @Override
            public void addIntHeader(String s, int i) {

            }

            @Override
            public void setStatus(int i) {

            }

            @Override
            public void setStatus(int i, String s) {

            }

            @Override
            public int getStatus() {
                return 0;
            }

            @Override
            public String getHeader(String s) {
                return null;
            }

            @Override
            public Collection<String> getHeaders(String s) {
                return null;
            }

            @Override
            public Collection<String> getHeaderNames() {
                return null;
            }

            @Override
            public String getCharacterEncoding() {
                return null;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public ServletOutputStream getOutputStream() throws IOException {
                return null;
            }

            @Override
            public PrintWriter getWriter() throws IOException {
                return null;
            }

            @Override
            public void setCharacterEncoding(String s) {

            }

            @Override
            public void setContentLength(int i) {

            }

            @Override
            public void setContentLengthLong(long l) {

            }

            @Override
            public void setContentType(String s) {

            }

            @Override
            public void setBufferSize(int i) {

            }

            @Override
            public int getBufferSize() {
                return 0;
            }

            @Override
            public void flushBuffer() throws IOException {

            }

            @Override
            public void resetBuffer() {

            }

            @Override
            public boolean isCommitted() {
                return false;
            }

            @Override
            public void reset() {

            }

            @Override
            public void setLocale(Locale locale) {

            }

            @Override
            public Locale getLocale() {
                return null;
            }
        };
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(name);
        CellStyle textStyle = workbook.createCellStyle();
        HSSFDataFormat format = workbook.createDataFormat();
        textStyle.setDataFormat(format.getFormat("@"));
        for (int i = 0; i < paramList.size(); i++) {
            HSSFRow row = sheet.createRow(i);
            for (int i1 = 0; i1 < paramList.get(i).size(); i1++) {
                HSSFCell hssfCell = row.createCell(i1);
                hssfCell.setCellStyle(textStyle);
                hssfCell.setCellValue(paramList.get(i).get(i1));
                sheet.setDefaultColumnStyle(i1, textStyle);
            }
        }

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            byte[] barray = bos.toByteArray();
            InputStream in = new ByteArrayInputStream(barray);


            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(name, "UTF-8"));

            OutputStream out1;
            out1 = response.getOutputStream();
            IOUtils.copy(in, out1);
            out1.flush();
            workbook.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}

package com.esgcc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(basePackageClasses = Cy.class)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AppConfig {

}
